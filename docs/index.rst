.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :maxdepth: 4

   operations
   development
   api
