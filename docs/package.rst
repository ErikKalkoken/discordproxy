Python package
==============

Modules of the Discord Proxy package that are relevant for clients.

Client
--------------

.. automodule:: discordproxy.client
    :members:

Exceptions
--------------

.. automodule:: discordproxy.exceptions
    :members:

gRPC classes
------------

.. automodule:: discordproxy.discord_api_pb2_grpc
    :members:

.. automodule:: discordproxy.discord_api_pb2
    :members:

Helpers
--------------

.. automodule:: discordproxy.helpers
    :members:
